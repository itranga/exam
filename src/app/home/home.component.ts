import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

import { HotelsService } from './../services/hotels.service';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  hotels: any = [];
  isLoading = false;
  private searchActive = false;
  keyword = 'name';
  data: any = [];
  from: string;
  to: string;
  city: string;

  constructor(private hotelsService: HotelsService, private router: Router) {}

  counter(i: number) {
    return new Array(Math.floor(i));
  }

  selectEvent(item: any) {
    this.city = item.id;
  }

  onChangeSearch(val: string) {
    this.hotelsService
      .getLocation(val)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(hotels => {
        this.data = hotels;
      });
  }

  onFocused(e: any) {
    // do something when input is focused
  }

  appendLeadingZeroes(n: string | number) {
    if (n <= 9) {
      return '0' + n;
    }
    return n;
  }

  ngOnInit() {
    let self = this;

    $(document).ready(function() {
      self.initHomeSlider();
      self.initSearchForm();

      $('.datepicker').datepicker({
        onSelect: function() {
          let dateObject = $(this).datepicker('getDate');

          let msec = Date.parse(dateObject);
          let current_datetime = new Date(msec);
          let formatted_date =
            self.appendLeadingZeroes(current_datetime.getDate()) +
            '-' +
            self.appendLeadingZeroes(current_datetime.getMonth() + 1) +
            '-' +
            current_datetime.getFullYear();
          if (this.id == 'check_in') {
            self.from = formatted_date;
          } else {
            self.to = formatted_date;
          }
        }
      });
    });
    this.isLoading = true;

    this.hotelsService
      .getAllHotels()
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(hotels => {
        this.hotels = hotels;

        let shuffled = this.hotels.sort(function() {
          return 0.5 - Math.random();
        });
        let selected = shuffled.slice(0, 30);
        this.hotels = selected;
      });
  }

  initSearchForm() {
    const self = this;
    if ($('.search_form').length) {
      var searchForm = $('.search_form');
      var searchInput = $('.search_content_input');
      var searchButton = $('.content_search');

      searchButton.on('click', function(event: any) {
        event.stopPropagation();

        if (!self.searchActive) {
          searchForm.addClass('active');
          self.searchActive = true;

          $(document).one('click', function closeForm(e: any) {
            if ($(e.target).hasClass('search_content_input')) {
              $(document).one('click', closeForm);
            } else {
              searchForm.removeClass('active');
              self.searchActive = false;
            }
          });
        } else {
          searchForm.removeClass('active');
          self.searchActive = false;
        }
      });
    }
  }

  initHomeSlider() {
    const self = this;
    if ($('.home_slider').length) {
      var homeSlider = $('.home_slider');

      homeSlider.owlCarousel({
        items: 1,
        loop: true,
        autoplay: false,
        smartSpeed: 1200,
        dotsContainer: 'main_slider_custom_dots'
      });

      /* Custom nav events */
      if ($('.home_slider_prev').length) {
        var prev = $('.home_slider_prev');

        prev.on('click', function() {
          homeSlider.trigger('prev.owl.carousel');
        });
      }

      if ($('.home_slider_next').length) {
        var next = $('.home_slider_next');

        next.on('click', function() {
          homeSlider.trigger('next.owl.carousel');
        });
      }

      /* Custom dots events */
      if ($('.home_slider_custom_dot').length) {
        $('.home_slider_custom_dot').on('click', function() {
          $('.home_slider_custom_dot').removeClass('active');
          $(this).addClass('active');
          homeSlider.trigger('to.owl.carousel', [$(this).index(), 300]);
        });
      }

      /* Change active class for dots when slide changes by nav or touch */
      homeSlider.on('changed.owl.carousel', function(event: any) {
        $('.home_slider_custom_dot').removeClass('active');
        $('.home_slider_custom_dots li')
          .eq(event.page.index)
          .addClass('active');
      });

      // add animate.css class(es) to the elements to be animated

      // Fired before current slide change
      homeSlider.on('change.owl.carousel', function(event: any) {
        var $currentItem = $('.home_slider_item', homeSlider).eq(event.item.index);
        var $elemsToanim = $currentItem.find('[data-animation-out]');
        self.setAnimation($elemsToanim, 'out');
      });

      // Fired after current slide has been changed
      homeSlider.on('changed.owl.carousel', function(event: any) {
        var $currentItem = $('.home_slider_item', homeSlider).eq(event.item.index);
        var $elemsToanim = $currentItem.find('[data-animation-in]');
        self.setAnimation($elemsToanim, 'in');
      });
    }
  }

  setAnimation(_elem: { each: (arg0: () => void) => void }, _InOut: string) {
    // Store all animationend event name in a string.
    // cf animate.css documentation
    var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

    _elem.each(function() {
      var $elem = $(this);
      var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

      $elem.addClass($animationType).one(animationEndEvent, function() {
        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
      });
    });
  }

  register(form: { value: any }) {
    this.router.navigate([
      '/hotels/' + this.city + '/' + this.from + '/' + this.to + '/' + form.value.adults + '/' + form.value.children
    ]);
  }
}
