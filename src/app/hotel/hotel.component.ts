import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { HotelsService } from './../services/hotels.service';

declare var $: any;
declare var google: any;

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {
  hotels: any = [];
  hotelsFacilities: any = [];

  hotelObj: any = null;
  backgroundUrl = 'http://72.249.104.22:8080/dnata-integration';
  imgPath = 'http://72.249.104.22:8080/dnata-integration';
  price: any = 0;
  rooms: any = [];
  pic: any = [];

  constructor(private route: ActivatedRoute, private hotelsService: HotelsService) {}

  ngOnInit() {
    let self = this;

    if (sessionStorage.getItem('price')) {
      this.price = sessionStorage.getItem('price');
    }

    this.route.paramMap.subscribe(params => {
      const hotelId = this.route.snapshot.params.id;

      this.hotelsService
        .roomPictures(hotelId)
        .pipe(finalize(() => {}))
        .subscribe(hotelsRoomsPic => {
          const hotelsRoomsPic1 = JSON.parse(JSON.stringify(hotelsRoomsPic));

          this.hotelsService
            .getRooms(hotelId)
            .pipe(finalize(() => {}))
            .subscribe(hotelsRooms => {
              let hotelsRooms1 = JSON.parse(JSON.stringify(hotelsRooms));

              hotelsRooms1[0].info.forEach(function(room: any) {
                const roomType = room.roomTypeName;
                let found = hotelsRoomsPic1.find(function(element: any) {
                  return element.roomTye == roomType;
                });

                if (found) {
                  let pic = 'data:image/png;base64,' + found.pictures[0];

                  room.picture = pic;
                } else {
                  room.picture = null;
                }

                console.log(room);

                self.rooms.push(room);
              });
            });
        });

      this.hotelsService
        .getHotelFacilities(hotelId)
        .pipe(finalize(() => {}))
        .subscribe(hotelsFacilities => {
          this.hotelsFacilities = hotelsFacilities;
        });

      this.hotelsService
        .getAllHotelPictures(hotelId)
        .pipe(finalize(() => {}))
        .subscribe(hotelPictures => {
          this.pic = hotelPictures;
          $(document).ready(function() {
            self.initListingSlider();
          });
        });

      this.hotelsService
        .getAllHotels()
        .pipe(finalize(() => {}))
        .subscribe(hotels => {
          this.hotels = hotels;

          for (var i = 0; i < this.hotels.length; i++) {
            if (this.hotels[i].hotelId == hotelId) {
              this.hotelObj = this.hotels[i];
              break;
            }
          }

          console.log('ranga', this.hotelObj);

          this.backgroundUrl = this.backgroundUrl + this.hotelObj.picture;
          this.backgroundUrl = this.backgroundUrl.replace(' ', '%20');

          $(document).ready(function() {
            $('.parallax-window').parallax();

            self.initLightbox();

            self.initGoogleMap(self.hotelObj.latitude, self.hotelObj.longitude);
          });
        });
    });
  }

  initGoogleMap(lat: any, long: any) {
    // lat = 36.132229;
    // long  = 5.351153

    var myLatlng = new google.maps.LatLng(lat, -long);
    var mapOptions = {
      center: myLatlng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      draggable: true,
      scrollwheel: false,
      zoomControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: true
    };

    // Initialize a map with options
    let map = new google.maps.Map(document.getElementById('map'), mapOptions);

    // Re-center map after window resize
    google.maps.event.addDomListener(window, 'resize', function() {
      setTimeout(function() {
        google.maps.event.trigger(map, 'resize');
        map.setCenter(myLatlng);
      }, 1400);
    });
  }

  initLightbox() {
    if ($('.cboxElement').length) {
      $('.colorbox').colorbox({
        rel: 'colorbox'
      });
    }
  }

  initListingSlider() {
    if ($('.hotel_slider').length) {
      var hotelSlider = $('.hotel_slider');

      hotelSlider.owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        margin: 16,
        responsive: {
          0: { items: 2 },
          320: { items: 3 },
          480: { items: 4 },
          575: { items: 5 },
          768: { items: 7 },
          992: { items: 8 },
          1199: { items: 9 }
        }
      });

      /* Custom nav events */
      if ($('.hotel_slider_prev').length) {
        var prev = $('.hotel_slider_prev');

        prev.on('click', function() {
          hotelSlider.trigger('prev.owl.carousel');
        });
      }

      if ($('.hotel_slider_next').length) {
        var next = $('.hotel_slider_next');

        next.on('click', function() {
          hotelSlider.trigger('next.owl.carousel');
        });
      }
    }
  }
}
