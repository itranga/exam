import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { HotelsService } from './../services/hotels.service';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})
export class HotelsComponent implements OnInit {
  isLoading = false;
  hotels: any = [];

  constructor(private hotelsService: HotelsService, private route: ActivatedRoute) {}

  ngOnInit() {
    $(document).ready(function() {
      $('.parallax-window').parallax();
    });

    let self = this;

    this.isLoading = true;

    this.route.paramMap.subscribe(params => {
      const hotelId = this.route.snapshot.params.id;
      const from = this.route.snapshot.params.from;
      const to = this.route.snapshot.params.to;
      const adults = this.route.snapshot.params.adults;
      const children = this.route.snapshot.params.children;

      sessionStorage.setItem('from', from);
      sessionStorage.setItem('to', to);
      sessionStorage.setItem('adults', adults);
      sessionStorage.setItem('children', children);

      this.hotelsService
        .findHotelByCity(hotelId, from, to, adults, children)
        .pipe(
          finalize(() => {
            this.isLoading = false;
          })
        )
        .subscribe(hotels => {
          let hotelObjs = [];

          let hotelsdata = JSON.parse(JSON.stringify(hotels));

          hotelsdata.forEach(function(element: any) {
            if (element.status == 1) {
              self.hotelsService
                .getAllHotels()
                .pipe(finalize(() => {}))
                .subscribe((hotels1: any) => {
                  let hotelObj = null;

                  for (var i = 0; i < hotels1.length; i++) {
                    if (hotels1[i].hotelId == hotelId) {
                      hotels1[i].price = element.price;
                      hotelObj = hotels1[i];
                      break;
                    }
                  }

                  self.hotels.push(hotelObj);
                });
            }
          });
        });
    });

    // this.hotelsService
    //   .getAllHotels()
    //   .pipe(
    //     finalize(() => {
    //       this.isLoading = false;
    //     })
    //   )
    //   .subscribe(hotels => {
    //     this.hotels = hotels;
    //   });
  }

  counter(i: number) {
    return new Array(Math.floor(i));
  }
}
