import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

const routes = {
  findAll: () => `/v1/hotel/getHotelDetails`,
  findHotelByCity: () => `/v1/hotel/getHotelAvailability`,
  getRooms: () => `/v1/hotel/getHotelRoomTypesAndPrice`,
  findOne: (str: String) => `/v1/hotel/getHotelPictures/` + str,
  roomPictures: (str: String) => `/v1/hotel/getHotelRoomTypesAndPictures/` + str,
  getHotelFacilities: (str: String) => `/v1/hotel/getHotelFacilities/` + str,
  getLocation: (str: String) => `/v1/hotel/findHotelList/` + str + `%25`
};

export interface Hotel {
  category: string;
}

@Injectable()
export class HotelsService {
  private httpOptions: any;

  constructor(private httpClient: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
  }

  getAllHotels(): Observable<string> {
    return this.httpClient
      .cache()
      .get(routes.findAll(), this.httpOptions)
      .pipe(map((body: any) => body));
  }

  roomPictures(str: String): Observable<string> {
    return this.httpClient
      .cache()
      .get(routes.roomPictures(str), this.httpOptions)
      .pipe(map((body: any) => body));
  }

  getAllHotelPictures(str: String): Observable<string> {
    return this.httpClient
      .cache()
      .get(routes.findOne(str), this.httpOptions)
      .pipe(map((body: any) => body));
  }

  getLocation(str: String): Observable<string> {
    return this.httpClient
      .cache()
      .get(routes.getLocation(str), this.httpOptions)
      .pipe(map((body: any) => body));
  }

  getHotelFacilities(str: String): Observable<string> {
    return this.httpClient
      .cache()
      .get(routes.getHotelFacilities(str), this.httpOptions)
      .pipe(map((body: any) => body));
  }

  getRooms(hotelId: String): Observable<string> {
    const hero = {
      hotelId: [hotelId],
      checkInDate: sessionStorage.getItem('from'),
      checkOutDate: sessionStorage.getItem('to'),
      agentID: 626,
      numberOfRooms: 1,
      roomList: [
        {
          room: '1',
          adults: sessionStorage.getItem('adults'),
          child: sessionStorage.getItem('children'),
          childAge: new Array()
        }
      ]
    };

    return this.httpClient
      .cache()
      .post(routes.getRooms(), hero, this.httpOptions)
      .pipe(map((body: any) => body));
  }

  findHotelByCity(hotelId: String, from: String, to: String, adults: Number, children: Number): Observable<string> {
    const hero = {
      hotelId: [hotelId],
      checkInDate: from,
      checkOutDate: to,
      agentID: 626,
      numberOfRooms: 1,
      roomList: [
        {
          room: '1',
          adults: adults,
          child: children,
          childAge: new Array()
        }
      ]
    };

    return this.httpClient
      .cache()
      .post(routes.findHotelByCity(), hero, this.httpOptions)
      .pipe(map((body: any) => body));
  }
}
