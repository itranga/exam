import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'hotel-item',
  templateUrl: './hotelItem.component.html',
  styleUrls: ['./hotelItem.component.scss']
})
export class HotelItemComponent implements OnInit {
  @Input() item: any;
  backgroundUrl = 'http://72.249.104.22:8080/dnata-integration/';

  constructor() {}

  ngOnInit() {
    if (this.item.price) {
      sessionStorage.setItem('price', this.item.price);
    }

    this.backgroundUrl = this.backgroundUrl + this.item.picture;
    this.backgroundUrl = this.backgroundUrl.replace(/ /g, '%20');
  }

  counter(i: number) {
    return new Array(Math.floor(i));
  }
}
