import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HotelsService } from './../services/hotels.service';

import { AutocompleteLibModule } from 'angular-ng-autocomplete';

import { HotelItemModule } from './../shared/hotelItem.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    FormsModule,
    SharedModule,
    HomeRoutingModule,
    AutocompleteLibModule,
    HotelItemModule
  ],
  declarations: [HomeComponent],
  providers: [HotelsService]
})
export class HomeModule {}
