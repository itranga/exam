import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { AboutRoutingModule } from './hotels-routing.module';
import { HotelsComponent } from './hotels.component';
import { HotelsService } from './../services/hotels.service';

import { HotelItemModule } from './../shared/hotelItem.module';

@NgModule({
  imports: [CommonModule, TranslateModule, AboutRoutingModule, HotelItemModule],
  declarations: [HotelsComponent],
  providers: [HotelsService]
})
export class HotelsModule {}
