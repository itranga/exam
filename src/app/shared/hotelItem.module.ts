import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HotelItemComponent } from './hotelItem/hotelItem.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [HotelItemComponent],
  exports: [HotelItemComponent]
})
export class HotelItemModule {}
