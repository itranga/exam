import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AppInitService {
  constructor(private httpClient: HttpClient) {}

  Init(): Promise<any> {
    console.log(`getSettings:: before http.get call`);

    const loginHttp = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
      })
    };

    const user = {
      username: 'admin',
      password: '1qazxsw2'
    };

    const promise = this.httpClient
      .post('/auth/login', user, loginHttp)
      .toPromise()
      .then(settings => {
        localStorage.setItem('token', settings['token']);
        return settings;
      });

    return promise;
  }
}
