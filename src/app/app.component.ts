import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { merge } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import { environment } from '@env/environment';
import { Logger, I18nService, untilDestroyed } from '@app/core';

const log = new Logger('App');

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private menuActive = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private translateService: TranslateService,
    private i18nService: I18nService
  ) {}

  ngOnInit() {
    // Setup logger
    if (environment.production) {
      Logger.enableProductionMode();
    }

    this.router.events.subscribe(evt => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });

    const self = this;

    $(document).ready(function() {
      self.setHeader();
      self.initMenu();

      $(window).on('resize', function() {
        self.setHeader();
      });

      $(document).on('scroll', function() {
        self.setHeader();
      });
    });

    log.debug('init');

    // Setup translations
    this.i18nService.init(environment.defaultLanguage, environment.supportedLanguages);

    const onNavigationEnd = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    // Change page title on navigation or language change, based on route data
    merge(this.translateService.onLangChange, onNavigationEnd)
      .pipe(
        map(() => {
          let route = this.activatedRoute;
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter(route => route.outlet === 'primary'),
        switchMap(route => route.data),
        untilDestroyed(this)
      )
      .subscribe(event => {
        const title = event['title'];
        if (title) {
          this.titleService.setTitle(this.translateService.instant(title));
        }
      });
  }

  setHeader() {
    const self = this;
    var header = $('.header');
    if (window.innerWidth < 992) {
      if ($(window).scrollTop() > 100) {
        header.addClass('scrolled');
      } else {
        header.removeClass('scrolled');
      }
    } else {
      if ($(window).scrollTop() > 100) {
        header.addClass('scrolled');
      } else {
        header.removeClass('scrolled');
      }
    }
    if (window.innerWidth > 991 && self.menuActive) {
      self.closeMenu();
    }
  }

  initMenu() {
    const self = this;
    if ($('.hamburger').length && $('.menu').length) {
      var hamb = $('.hamburger');
      var close = $('.menu_close_container');

      hamb.on('click', function() {
        if (!self.menuActive) {
          self.openMenu();
        } else {
          self.closeMenu();
        }
      });

      close.on('click', function() {
        if (!self.menuActive) {
          self.openMenu();
        } else {
          self.closeMenu();
        }
      });
    }
  }

  openMenu() {
    var menu = $('.menu');
    menu.addClass('active');
    this.menuActive = true;
  }

  closeMenu() {
    var menu = $('.menu');
    menu.removeClass('active');
    this.menuActive = false;
  }

  ngOnDestroy() {
    this.i18nService.destroy();
  }
}
